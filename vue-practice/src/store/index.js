import Vue from 'vue';
import Vuex from 'vuex';
import login from './login';
import task from './task';
import covid from './covid';
import details from './details';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        login,
        task,
        covid,
        details,
    },
});
