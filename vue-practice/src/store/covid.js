import axios from 'axios';

export default {
    state: {
        covid: null,
    },
    mutations: {
        setCovid(state, covid) {
            state.covid = covid;
        },
    },
    actions: {
        getCovid({ commit }) {
            return axios

                .get('https://api.covid19api.com/summary')

                .then((response) => {
                    console.log(response.data, 'covid stats respone');
                    commit('setCovid', response.data.Countries);
                })
                .catch((err) => console.log(err));
        },
    },
    getters: {
        covid(state) {
            return state.covid;
        },
    },
};
