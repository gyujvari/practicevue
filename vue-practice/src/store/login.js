import axios from 'axios';
export default {
    state: {
        isLoggedIn: false,
        isUserUnautorized: false,
    },
    getters: {
        getLoginState(state) {
            return state.isLoggedIn;
        },
        getUserState(state) {
            return state.isUserUnautorized;
        },
    },
    mutations: {
        setLoggedIn(state, payload) {
            state.isLoggedIn = payload;
        },
        setUserUnaut(state, payload) {
            state.isUserUnautorized = payload;
        },
    },
    actions: {
        sendCredentials({ commit }, payload) {
            axios
                .post('https://reqres.in/api/login', payload)
                .then(function(response) {
                    console.log(response);
                    commit('setLoggedIn', true);
                })
                .catch(function(error) {
                    console.log('Show error notification!');
                    commit('setUserUnaut', true);
                    return Promise.reject(error);
                });
        },
    },
};
