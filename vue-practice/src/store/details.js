import axios from 'axios';

export default {
    state: {
        details: null,
    },
    getters: {
        details(state) {
            return state.details;
        },
    },
    mutations: {
        setDetails(state, details) {
            state.details = details;
        },
    },
    actions: {
        getDetailsByCode({ commit }, Slug) {
            return axios

                .get('https://api.covid19api.com/country/' + Slug + '/status/confirmed/live?from=2020-03-01T00:00:00Z&to=2020-04-01T00:00:00Z')

                .then((response) => {
                    console.log(response.data, 'daily stats');
                    commit('setDetails', response.data);
                })
                .catch((err) => console.log(err));
        },
    },
};
