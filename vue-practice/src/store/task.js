import axios from 'axios';

export default {
    state: {
        tasks: null,
        isResponse: false,
    },
    getters: {
        tasks(state) {
            return state.tasks;
        },
        getResponseValue(state) {
            return state.isResponse;
        },
    },
    mutations: {
        setTasks(state, tasks) {
            state.tasks = tasks;
        },
        setResponse(state) {
            state.isResponse = true;
        },
    },
    actions: {
        getTasks({ commit }) {
            axios
                .get(process.env.VUE_APP_API_URL)
                .then((response) => {
                    commit('setTasks', response.data);
                    commit('setResponse');
                })
                .catch((err) => console.log(err));
        },
    },
};
