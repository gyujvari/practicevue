import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/promise',
        name: 'Promise',

        component: () => import('../components/Promise.vue'),
    },

    {
        path: '/vueonline',
        name: 'Online',

        component: () => import('../components/OnlineProp.vue'),
    },
    {
        path: '/geolocation',
        name: 'Location',

        component: () => import('../components/Geolocation.vue'),
    },
    {
        path: '/inputcolor',
        name: 'Inputcolor',

        component: () => import('../components/InputColor.vue'),
    },
    {
        path: '/dynamicsize',
        name: 'DynamicSize',

        component: () => import('../components/DynamicSize.vue'),
    },
    {
        path: '/bindstyle',
        name: 'bind',

        component: () => import('../components/AppDivsObj.vue'),
    },
    {
        path: '/todo',
        name: 'Todo',

        component: () => import('../components/Todo.vue'),
    },
    {
        path: '/refs',
        name: 'Refs',

        component: () => import('../components/Refs.vue'),
    },
    {
        path: '/fetch',
        name: 'Fetch',

        component: () => import('../components/Fetch.vue'),
    },
    {
        path: '/cssgrid',
        name: 'cssgrid',

        component: () => import('../components/CssGrid.vue'),
    },
    {
        path: '/parent',
        name: 'Parent',

        component: () => import('../components/Parent.vue'),
    },
    {
        path: '/child',
        name: 'Child',

        component: () => import('../components/Child.vue'),
    },
    {
        path: '/slots',
        name: 'Slots',

        component: () => import('../components/Slot.vue'),
    },
    {
        path: '/customdirective',
        name: 'Custom',

        component: () => import('../components/CustomDirective.vue'),
    },
    {
        path: '/mixins',
        name: 'mixin',

        component: () => import('../components/CustomDirective.vue'),
    },
    {
        path: '/covidstats',
        name: 'covidstats',

        component: () => import('../components/CovidStats.vue'),
    },
    {
        path: '/coviddetails',
        name: 'covidDetail',

        component: () => import('../components/CovidDetails.vue'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
