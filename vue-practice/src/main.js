import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import store from './store';
import router from './router';
import Nest from './components/NestComponent.vue';
import './registerServiceWorker'

Vue.component('nest', Nest);

Vue.config.productionTip = false;
Vue.directive('rainbow', {
    bind(el) {
        el.style.color =
            '#' +
            Math.random()
                .toString()
                .slice(2, 8);
    },
});

new Vue({
    vuetify,
    store,
    router,
    render: (h) => h(App),
}).$mount('#app');
